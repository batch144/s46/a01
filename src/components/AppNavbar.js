
import {Navbar, Nav} from "react-bootstrap";

export default function AppNavbar(){

	return(

		<Navbar bg="primary" expand="lg">
					<Navbar.Brand href="#home">React Booking</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse id="basic-navbar-nav">
						<Nav className="me-auto">
							<Nav.Link href="#home">Home</Nav.Link>
							<Nav.Link href="#courses">Courses</Nav.Link>
						</Nav>
					</Navbar.Collapse>
		</Navbar>
	)
}