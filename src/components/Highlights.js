import {Container, Row, Col, Card} from "react-bootstrap";

export default function Highlights(){

	return(

		<Container fluid className="mb-4">
			<Row>
				{/*card 1*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Learn from Home</Card.Title>
					    <Card.Text>
					      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
				{/*card 2*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Study Now, Pay Later</Card.Title>
					    <Card.Text>
					      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
				{/*card 2*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights p-3">
					  <Card.Body>
					    <Card.Title>Be Part of Our Community</Card.Title>
					    <Card.Text>
					      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}