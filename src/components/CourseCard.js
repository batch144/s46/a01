import { useState } from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}){
	console.log(courseProp)	//object

	const {name, description, price} = courseProp

	//Use the state hook for this component to be able to store its state
	//States are used to keep track of information related to individual components
	/*
	Syntax:
		const [getter, setter] = useState(initialGetterValue)
	*/
	const [count, setCount] = useState(0)
	console.log(useState(0))

	const [seat, setSeat] = useState(10)

	function enroll(){
		if(count < 10 && seat !== 0){
			setCount(count + 1);
			setSeat(seat - 1);
			console.log('Enrollees: ' +count, +seat)
		} else{
			alert("No more seats.")
		}
	}

	return(
		<Container fluid className="mb-4">
			<Row className="justify-content-center">
				<Col xs={10} md={8}>
					<Card className="p-4">
						<Card.Title>{name}</Card.Title>
						<Card.Body>
							<Card.Text>Description:</Card.Text>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							<Card.Text>Enrollees: {count}</Card.Text>
							<Button variant="primary" onClick={enroll}>Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}


//check if the CourseCard component is getting the correct prop types
//Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.

CourseCard.propTypes = {
	//shape method => is used to check if a prop conforms to a specific "shape"
	courseProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
